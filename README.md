# Bluedot SDK Demo Application

Create a Demo Integration App using the Web SDK.
A project to gain further insights from the developers side and document findings.

## Documentation
Documentation:
https://paper.dropbox.com/doc/Bluedot-SDK-Demo-Integration-App--Bg3nxHRAmgF8phGynXLUJd~iAg-v5o8p94YqUX2bC4OqZTDu

## Features

| Source | Link | 
| ------ | ------ |
| Demo Web App | https://bluedot-test-app.herokuapp.com/ | 
| Source code | https://gitlab.com/kylegallard/sdk-demo-curbside |

- The “SDK Order” tab takes the user through the custom process created via the SDK. Clicking “Get Started” on the homepage also takes the user through this process.

- The “Now Ready” tab takes the user through the Hello Screens in a separate tab.


## Installation

Install the dependencies and devDependencies and start the server.

```sh
cd sdk-demo-curbside
npm install
npm start
```
