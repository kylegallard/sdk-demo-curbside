import { BrowserRouter, Routes, Route } from "react-router-dom";
import CustomerArrival from "../Project/CustomerInterface/CustomerArrival/CustomerArrival";
import CustomerInterface from "../Project/CustomerInterface/CustomerInterface";
import GetStarted from "../Project/GetStarted/GetStarted";

import CreateOrder from "../Project/CreateOrder/CreateOrder";
import Navbar from "../Common/components/Navbar/Navbar";
import Footer from "../Common/components/Footer/Footer";

const Router = () => {
    return (
        <BrowserRouter>
            <Navbar />
            <Routes>
                <Route path="/" element={<GetStarted />}></Route>
                <Route path="/now-ready" element={<CreateOrder />}></Route>
                <Route path="/sdk-order" element={<CustomerInterface />}></Route>
                <Route path="/order" element={<CustomerArrival />}></Route>
            </Routes>
            <Footer />
        </BrowserRouter>
    );
};

export default Router;