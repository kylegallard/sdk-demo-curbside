import { useEffect, useState } from 'react';
import Router from "./Router";
const bluedotSdk = require("@bluedot-innovation/javascript-sdk");
const {
  bluedot
} = bluedotSdk;

const App = () => {
  const [intialized, setInitialized] = useState(false);

  const initialize = async () => {
    try {
      await bluedot.initialize(process.env.REACT_APP_INITIALIZE);
      console.log("Successfully intialized.");
    } catch (error) {
      console.log(error);
    };
  };

  useEffect(() => {
    if (!intialized) {
      setInitialized(true);
      initialize(process.env.REACT_APP_INITIALIZE);
      bluedot.config.setUserPersistenceEnabled(true);
    }
  }, [intialized]);

  return (
    <Router />
  );
};

export default App;
