export const getGeolocation = () => {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
            console.log("Geolocation Latitude", position.coords.latitude);
            console.log("Geolocation Longitude", position.coords.longitude);
            console.log("Geolocation timestamp", position.timestamp);
        }, (error) => {
            return console.log(error)
        });
    };
};