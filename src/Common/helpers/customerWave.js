const bluedotSdk = require("@bluedot-innovation/javascript-sdk");
const { createHelloModel, bluedot } = bluedotSdk;

export const customerDepartureWave = async (customerInfo, isErrorHandler, isError, isUserRegisteredHandler) => {
    const { eventType, orderId, name, number, parkingBay, food, drink } = customerInfo;

    const helloModel = createHelloModel();
    helloModel
        .setOrderId(orderId)
        .setEventType(eventType)
        .setMobileNumber(+number)
        .setCustomerName(name)
        .setDisplayedCustomDataField("Parking Bay", `${+parkingBay}`)
        .setDisplayedCustomDataField("Food", `${food}`)
        .setDisplayedCustomDataField("Drink", `${drink}`);
    const destinationId = "melbourne";

    try {
        await bluedot.wave.send(destinationId, helloModel);
    } catch (error) {
        isErrorHandler(error);
        console.log(error);
    };
    if (!isError) {
        isUserRegisteredHandler();
    };
};

export const customerArrivalWave = (orderId, eventType) => {
    let arrived = false;

    return new Promise((resolve, reject) => {
        const helloModel = createHelloModel();
        helloModel
            .setOrderId(orderId)
            .setEventType(eventType);
        const destinationId = "melbourne";

        try {
            bluedot.wave.send(destinationId, helloModel);
            arrived = true;
            resolve(arrived);
        } catch (error) {
            console.log(error);
            arrived = false;
            reject(arrived);
        }
    });
};