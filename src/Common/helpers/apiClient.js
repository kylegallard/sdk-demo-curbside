import { v4 as uuidv4 } from 'uuid';

export const apiClient = async (input, loadingHandler) => {
    const { name, number, food, drink, parkingBay } = input;

    const baseUrl = 'https://au1-events.bluedot.io/hello/register';
    const apiKey = process.env.REACT_APP_INITIALIZE;
    const location = "melbourne";
    const orderId = uuidv4();

    const options = {
        method: "POST",
        headers: {
            "content-type": "application/json",
            "x-bluedot-api-key": apiKey
        },
        body: JSON.stringify({
            destinationId: location,
            customEventMetaData: {
                hs_OrderId: orderId,
                hs_CustomerName: name,
                hs_MobileNumber: number,
                hs_Food: food,
                hs_Drink: drink,
                hs_ParkingBay: parkingBay
            },
            eventTime: new Date()
        })
    };

    loadingHandler(true);
    try {
        const response = await fetch(baseUrl, options);
        if (response.status === 200 || response.status === 201) {
            const data = await response.json();
            window.open(`https://au.nowready.io/${data.userToken}`);
        }
    } catch (err) {
        console.log(err.message);
    };
    loadingHandler(false);
};

