import './Footer.css';

const Footer = () => {
    const currentYear = new Date().getFullYear();
    return (
        <footer id="footer">
            <div className="footer__container">
                <p className="copyright">©Bluedot - {currentYear}</p>
            </div>
        </footer>
    );
};

export default Footer;