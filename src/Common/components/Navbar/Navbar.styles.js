import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const Icon = styled.nav`
    display: none;

    @media screen and (max-width: 600px) {
    padding: 14px 16px;
    text-align: center;
    font-size: 20px;
    cursor: pointer;
  }
`;

export const Navlink = styled(Link)`
    padding: 14px 16px;
    text-align: center;
    font-family: 'League Spartan';
    font-size: 17px;
    font-weight: 600;
    color: #7b10f4;
`;

export const Nav = styled.nav`
    padding-left: 5%;
    padding-right: 5%;
    width: 100%;
    top: 0;
    position: fixed;
    display: flex;
    overflow: hidden;
    background-color: #F5F5F5;
    z-index: 200;

    ${Navlink}:hover {
        background-color: #7b10f4;
        color: white;
    }

    ${Navlink}:active {
        background-color: #7b10f4;
        color: white;
    }

    ${Navlink}:last-child {
        margin-left: auto;
        cursor: default;
    }

    ${Navlink}:first-child:hover {
        background-color: transparent;
        color: #7b10f4;
    }

    ${Navlink}:last-child:hover {
        background-color: transparent;
        color: #7b10f4;
        margin-left: auto;
    }

    @media screen and (max-width: 600px) {
        display: ${props => props.responsive && "flex"};
        text-align: ${props => props.responsive && "left"};
        flex-direction: ${props => props.responsive && "column"};
        
        ${Navlink}:not(:first-child) {
            display:  ${props => props.responsive ? "flex" : "none"};
        }
        
        ${Icon} {
            /* float: left; */
            display: flex;
            position: ${props => props.responsive && "relative"};
        }
    }
`;

