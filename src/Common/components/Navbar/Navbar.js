import { useState } from 'react';
import { Nav, Navlink, Icon } from './Navbar.styles';
import logo from '../../../assets/logo_lg.png';

const Navbar = () => {
    const [clicked, setClicked] = useState(false);

    const onClickNav = () => {
        setClicked(!clicked);
    };

    return (
        <>
            <Nav responsive={clicked} onClick={onClickNav}>
                <Navlink to="/" onClick={onClickNav}><img width={20} src={logo} alt="bluedot-logo"></img></Navlink>
                {clicked && (
                    <Icon className="icon" onClick={onClickNav}>
                        <i className="fa-solid fa-xmark"></i>
                    </Icon>
                )}
                <Navlink to="/" onClick={onClickNav}>Home</Navlink>
                <Navlink to="/now-ready" onClick={onClickNav}>Now Ready</Navlink>
                <Navlink to="/sdk-order" onClick={onClickNav}>SDK Order</Navlink>
                <Navlink to="/order" onClick={onClickNav}>Cart</Navlink>
                {!clicked && (
                    <Icon className="icon" onClick={onClickNav}>
                        <i className="fa fa-bars"></i>
                    </Icon>
                )}
                <Navlink to="#">DEMO APP</Navlink>
            </Nav>
        </>
    );
};

export default Navbar;