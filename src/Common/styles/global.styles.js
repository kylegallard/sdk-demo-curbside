import styled from 'styled-components';

export const Button = styled.button`
  font-family: 'League Spartan' !important;
  font-weight: 600 !important;
  background-color: #7b10f4 !important;
  color: white !important;

  &:hover {
    background-color: #7C4DFF !important;
  }
`;