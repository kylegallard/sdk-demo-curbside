import { useState } from "react";
import { apiClient } from "../../Common/helpers/apiClient";
import { Checkbox, Form, Dropdown, FormField } from 'semantic-ui-react'
import Loading from "./Loading";
import { Button } from "../../Common/styles/global.styles";
import "../CustomerInterface/CustomerRegistration/CustomerRegistration.css";

import { foodSelectionData, drinkSelectionData, parkingBaySelectionData } from "./CreateOrderFormData";

const CustomerRegistration = () => {
    const [loading, setLoading] = useState(false);
    const [input, setInput] = useState({
        name: "",
        number: "",
        parkingBay: "",
        food: "",
        drink: ""
    });

    const loadingHandler = (isLoading) => {
        setLoading(isLoading);
    };

    const onSubmitHandler = (e, input) => {
        e.preventDefault();
        apiClient(input, loadingHandler);
    };

    const inputHandler = (event, result) => {
        const { name, value } = result || event.target;
        setInput((prevValue) => {
            return {
                ...prevValue,
                [name]: value
            }
        });
    };

    return (
        <section id="contact-form-section" className="form-section">
            <div className="form__wrapper">
                {loading && <Loading />}
                {!loading && (
                    <Form onSubmit={(e) => onSubmitHandler(e, input)}>
                        <h2 className="ui header order__header">Now Ready Order</h2>
                        <FormField required>
                            <label>Name</label>
                            <input value={input.name} onChange={inputHandler} type="text" name="name" placeholder='Name'></input>
                        </FormField>
                        <FormField required>
                            <label>Contact Number</label>
                            <input value={input.number} onChange={inputHandler} type="number" name="number" placeholder='Number' />
                        </FormField>

                        <FormField required>
                            <label>Select Food</label>
                            <Dropdown
                                onChange={inputHandler}
                                name="food"
                                placeholder='Select Food'
                                value={input.food}
                                fluid
                                search
                                selection
                                options={foodSelectionData}
                            />
                        </FormField>

                        <FormField required>
                            <label>Select Drink</label>
                            <Dropdown
                                onChange={inputHandler}
                                name="drink"
                                placeholder='Select Drink'
                                value={input.drink}
                                fluid
                                search
                                selection
                                options={drinkSelectionData}
                            />
                        </FormField>

                        <FormField required>
                            <label>Select Parking Bay</label>
                            <Dropdown
                                onChange={inputHandler}
                                name="parkingBay"
                                placeholder='Select Parking Bay'
                                value={input.parkingBay}
                                fluid
                                search
                                selection
                                options={parkingBaySelectionData}
                            />
                        </FormField>

                        <FormField required>
                            <Checkbox label='I agree to the Terms and Conditions' />
                        </FormField>
                        <br />
                        <Button className="ui button" type='submit'>Submit</Button>
                    </Form>
                )}
            </div>
        </section>
    );
};

export default CustomerRegistration;


