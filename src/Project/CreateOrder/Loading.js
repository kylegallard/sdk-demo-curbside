import "../CustomerInterface/CustomerRegistration/CustomerRegistration.css";

const Loading = () => {
    return (
        <div style={{ width: "250px", height: "500px" }}>

            <p class="placeholder-glow">
                <span style={{ height: "30px" }} class="placeholder col-5 placeholder-lg"></span>
                <div style={{ height: "20px" }}></div>

                <span class="placeholder col-3 placeholder-md"></span>
                <div style={{ height: "10px" }}></div>
                <span style={{ height: "40px" }} class="placeholder col-12 placeholder-lg"></span>

                <div style={{ height: "30px" }}></div>
                <span class="placeholder col-5 placeholder-md"></span>
                <div style={{ height: "10px" }}></div>
                <span style={{ height: "40px" }} class="placeholder col-12 placeholder-lg"></span>

                <div style={{ height: "30px" }}></div>
                <span class="placeholder col-5 placeholder-md"></span>
                <div style={{ height: "10px" }}></div>
                <span style={{ height: "40px" }} class="placeholder col-12 placeholder-lg"></span>

                <div style={{ height: "30px" }}></div>
                <span class="placeholder col-6 placeholder-md"></span>
                <div style={{ height: "10px" }}></div>
                <span style={{ height: "40px" }} class="placeholder col-12 placeholder-lg"></span>

                <div style={{ height: "20px" }}></div>
                <span class="placeholder col-10 placeholder-sm"></span>
                <div style={{ height: "20px" }}></div>
                <span style={{ height: "30px" }} class="placeholder col-4 placeholder-lg"></span>
            </p>

        </div>
    );
};

export default Loading;