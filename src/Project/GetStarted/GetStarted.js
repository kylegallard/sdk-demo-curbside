import { Link } from "react-router-dom";
import image from '../../assets/Delivery-amico.svg';
import blueDotImg from '../../assets/bluedot_logo-default.svg';
import { Wrapper, Button } from "./GetStarted.styles";

const GetStarted = () => {
    return (
        <Wrapper>
            <img height={50} src={blueDotImg} alt="bluedot-logo-main"></img>
            <div><img src={image} alt="online-world" width="400"></img></div>
            <div>
                <Link to="/sdk-order"><Button className="ui button">Get Started</Button></Link>
            </div>
        </Wrapper>
    );
};

export default GetStarted;