import { useState, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { getGeolocation } from '../../Common/helpers/geolocation';
import { customerDepartureWave } from '../../Common/helpers/customerWave';
import CustomerRegistration from './CustomerRegistration/CustomerRegistration';
import { useNavigate } from "react-router-dom";

const CustomerInterface = () => {
    const [input, setInput] = useState({
        name: "",
        number: "",
        parkingBay: "",
        food: "",
        drink: ""
    });
    const [registered, setRegistered] = useState(false);
    const [isError, setIsError] = useState(false);

    let navigate = useNavigate();

    useEffect(() => {
        getGeolocation();
    }, []);

    const isErrorHandler = (err) => {
        if (err) {
            setIsError(!isError);
        };
    };

    const isUserRegisteredHandler = () => {
        setRegistered(!registered);
    };

    const inputHandler = (event, result) => {
        const { name, value } = result || event.target;
        setInput((prevValue) => {
            return {
                ...prevValue,
                [name]: value
            }
        });
    };

    const onSubmitHandler = (e, eventType, customerData) => {
        e.preventDefault();
        let orderId = uuidv4();
        let customerInfo = {
            eventType,
            orderId,
            name: customerData.name,
            number: customerData.number,
            parkingBay: customerData.parkingBay,
            food: customerData.food,
            drink: customerData.drink
        };
        customerDepartureWave(customerInfo, isErrorHandler, isError, isUserRegisteredHandler);
        navigate("/order", { state: { customerData: input, orderId: orderId } });
    };

    return (
        <div>
            <CustomerRegistration
                inputHandler={inputHandler}
                onSubmitHandler={onSubmitHandler}
                customerData={input}
            />
        </div>
    );
};

export default CustomerInterface;