import { foodSelectionData, drinkSelectionData, parkingBaySelectionData } from "./registrationFormData";
import { Checkbox, Form, Dropdown, FormField } from 'semantic-ui-react';
import { Button } from "../../GetStarted/GetStarted.styles";
import "./CustomerRegistration.css";

const CustomerRegistration = ({ inputHandler, onSubmitHandler, customerData }) => {
    const { name, number, parkingBay, food, drink } = customerData;

    return (
        <section id="contact-form-section" className="form-section">
            <div className="form__wrapper">
                <Form onSubmit={(e) => onSubmitHandler(e, "onTheWay", customerData)}>
                    <h2 className="ui header order__header">SDK Order</h2>
                    <FormField required>
                        <label>Name</label>
                        <input value={name} onChange={inputHandler} type="text" name="name" placeholder='Name'></input>
                    </FormField>
                    <FormField required>
                        <label>Contact Number</label>
                        <input value={number} onChange={inputHandler} type="number" name="number" placeholder='Number' />
                    </FormField>

                    <FormField required>
                        <label>Select Food</label>
                        <Dropdown
                            onChange={inputHandler}
                            name="food"
                            placeholder='Select Food'
                            value={food}
                            fluid
                            search
                            selection
                            options={foodSelectionData}
                        />
                    </FormField>

                    <FormField required>
                        <label>Select Drink</label>
                        <Dropdown
                            onChange={inputHandler}
                            name="drink"
                            placeholder='Select Drink'
                            value={drink}
                            fluid
                            search
                            selection
                            options={drinkSelectionData}
                        />
                    </FormField>

                    <FormField required>
                        <label>Select Parking Bay</label>
                        <Dropdown
                            onChange={inputHandler}
                            name="parkingBay"
                            placeholder='Select Parking Bay'
                            value={parkingBay}
                            fluid
                            search
                            selection
                            options={parkingBaySelectionData}
                        />
                    </FormField>

                    <FormField required>
                        <Checkbox label='I agree to the Terms and Conditions' />
                    </FormField>
                    <br />
                    <Button className="ui button" type='submit'>Submit</Button>
                </Form>
            </div>
        </section>
    );
};

export default CustomerRegistration;


