export const foodSelectionData = [
    {
        key: "none",
        text: "None",
        value: "None",
    },
    {
        key: "burger",
        text: "Burger",
        value: "Burger",
    },
    {
        key: "chips",
        text: "Chips",
        value: "Chips",
    },
    {
        key: "pizza",
        text: "Pizza",
        value: "Pizza",
    }
];

export const drinkSelectionData = [
    {
        key: "none",
        text: "None",
        value: "None",
    },
    {
        key: "coca-cola",
        text: "Coca Cola",
        value: "Coca Cola",
    },
    {
        key: "orange-juice",
        text: "Orange Juice",
        value: "Orange Juice",
    },
    {
        key: "latte",
        text: "Latte",
        value: "Latte",
    },
];

export const parkingBaySelectionData = [
    {
        key: "bay1",
        text: "Bay 1",
        value: "1",
    },
    {
        key: "bay2",
        text: "Bay 2",
        value: "2",
        disabled: true
    },
    {
        key: "bay3",
        text: "Bay 3",
        value: "3",
    },
    {
        key: "bay4",
        text: "Bay 4",
        value: "4",
        disabled: true
    },
    {
        key: "bay5",
        text: "Bay 5",
        value: "5",
        disabled: true
    },
    {
        key: "bay6",
        text: "Bay 6",
        value: "6",
    },
    {
        key: "bay7",
        text: "Bay 7",
        value: "7",
    },
    {
        key: "bay8",
        text: "Bay 8",
        value: "8",
    },
    {
        key: "bay9",
        text: "Bay 9",
        value: "9",
        disabled: true
    },
    {
        key: "bay10",
        text: "Bay 10",
        value: "10",
    },
];
