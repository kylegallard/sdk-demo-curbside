import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { displayUIHandler } from "./helpers";
import { customerArrivalWave } from "../../../Common/helpers/customerWave";
import Header from "./Header/Header";
import { MenuWrapper, FormWrapper, Heading, SubHeading } from "./CustomerArrival.styles";
import { HeaderContainer } from "./Header/Header.styles";
import empty from '../../../assets/Empty.gif';

const CustomerArrival = () => {
    const { state } = useLocation();
    if (state !== null) {
        var { customerData, orderId } = state;
        var { name, number, parkingBay, food, drink } = customerData;
    };
    const [data, setData] = useState(state);
    const [loader, setLoader] = useState(true);
    const [isActiveTab, setIsActiveTab] = useState(false);
    const [customerArrived, setCustomerArrived] = useState(false);

    useEffect(() => {
        window.scrollTo(0, 0);
    }, [state]);

    useEffect(() => {
        setTimeout(() => {
            if (loader) {
                setLoader(false);
            }
        }, 4000);
    }, [loader]);

    const isHeaderTabSelected = (e, value) => {
        e.preventDefault();
        setIsActiveTab(value);
    };

    const onArrivalSubmission = async () => {
        console.log("Customer has arrived");
        try {
            const arrived = await customerArrivalWave(orderId, "arrival");
            setCustomerArrived(arrived);
        } catch (error) {
            console.log(error);
        };
    };

    const customerUIHandler = () => {
        if (customerData !== undefined) {
            const customerArrival = displayUIHandler(
                customerData, isActiveTab, customerArrived, onArrivalSubmission, setCustomerArrived
            );
            return customerArrival;
        };
    };

    return (
        <div style={{ marginTop: "150px" }}>
            <HeaderContainer>
                <Header isActive={isActiveTab} isHeaderTabSelected={isHeaderTabSelected} />
            </HeaderContainer>
            <MenuWrapper>
                {data && state && isActiveTab && (
                    <FormWrapper>
                        <Heading>Order Details</Heading>
                        {loader && <div>Successfully Registered Order</div>}
                        <div>
                            {name && <h3>Hi {name}. Here is your current order:</h3>}
                            <p>Order Id: {orderId}</p>
                            <p>Phone no: {number}</p>
                            {parkingBay && <p>Parking bay: {parkingBay}</p>}
                            {food && <p>Food order: {food}</p>}
                            {drink && <p>Drink order: {drink}</p>}
                        </div>
                    </FormWrapper>
                )}
                {customerUIHandler()}
                {!state && (
                    <FormWrapper>
                        <SubHeading>There's nothing here...</SubHeading>
                        <img src={empty} width="300" alt="arrival-img"></img>
                    </FormWrapper>
                )}
            </MenuWrapper>
        </div>
    );
};

export default CustomerArrival;