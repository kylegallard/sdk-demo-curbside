import styled from 'styled-components';

export const HeaderWrapper = styled.section`
    width: 80%;
    padding: 0 !important;
    margin-top: -100px;
    min-width: 320px;
    width: 400px;
`;

export const HeaderContainer = styled.section`
    position: absolute;
    left: 0;
    right: 0;
    margin-left: auto;
    margin-right: auto;
    margin-top: 50px;
    width: 400px
`;