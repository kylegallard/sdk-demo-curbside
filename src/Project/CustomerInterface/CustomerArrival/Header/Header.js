import { Menu } from 'semantic-ui-react';
import { HeaderWrapper } from './Header.styles';

const Header = ({ isHeaderTabSelected, isActive }) => {
    return (
        <HeaderWrapper>
            <Menu secondary widths={2}>
                <Menu.Item
                    name='Arrival'
                    active={!isActive}
                    onClick={(e) => isHeaderTabSelected(e, false)}
                />
                <Menu.Item
                    name='Order Details'
                    active={isActive}
                    onClick={(e) => isHeaderTabSelected(e, true)}
                />
            </Menu>
        </HeaderWrapper>
    );
};

export default Header;