import { FormWrapper, Heading, Heading2, SubHeading } from "./CustomerArrival.styles";
import { Button } from "../../../Common/styles/global.styles";
import { Icon } from "semantic-ui-react";
import arrivalImg from '../../../assets/Take Away.gif';

export const displayUIHandler = (customerData, isActiveTab, customerArrived, onArrivalSubmission, setCustomerArrived) => {

    const { name, parkingBay } = customerData;

    let customerArrival;

    if (!isActiveTab && !customerArrived) {
        const bay = (<span>to <i><strong>Parking Bay {parkingBay}</strong>.</i></span>);
        customerArrival = (
            <FormWrapper>
                <Heading2><i>Hello, {name}!</i></Heading2>
                <SubHeading><Icon className="clock outline" />Your order is being prepared</SubHeading>
                <Heading>Ready to pickup?</Heading>
                <p>When you've arrived, please click the <i><strong>I've Arrived</strong></i> button below and we'll bring out your order{parkingBay && "."} {parkingBay && bay}</p>
                <Button className="ui button" onClick={onArrivalSubmission}>I've Arrived</Button>
            </FormWrapper>
        );
    };
    if (!isActiveTab && customerArrived) {
        customerArrival = (
            <FormWrapper>
                <Heading2><i>We'll be with you shortly!</i></Heading2>
                <img src={arrivalImg} width="300" alt="arrival-img"></img>
                <Button className="ui button" onClick={() => setCustomerArrived(false)} style={{ display: "flex", justifyContent: "center", marginLeft: "65px" }}>I've received my order</Button>
            </FormWrapper>
        );
    };
    return customerArrival;
};