import styled from 'styled-components';

export const MenuWrapper = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 15px;
  margin-top: 80px;
`;

export const FormWrapper = styled.div`
  border: 2px solid #E0E0E0;
  padding: 40px;
  border-radius: 10px;
  max-width: 420px;
`;

export const Heading = styled.h1`
  color: #7b10f4;
  font-size: 32px;
`;

export const Heading2 = styled.h1`
  font-size: 30px;
  color: #7b10f4;
  padding-bottom: 0;
  margin-bottom: 0;
`;

export const SubHeading = styled.h3`
    padding-top: 5px;
    margin: 0;
    font-size: 22px;
`;